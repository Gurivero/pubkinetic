﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BaseAI : MonoBehaviour {

	protected enum AI_ACTIONS
	{
		IDLE,
		PURSUE,
		ATTACK,
		DEFEND,
		KINETIC
	}


	public enum AI_TYPE
	{
		ATTACKER,
		DEFENDER,
		BALANCED
	}

	protected Player _player;
	protected BaseBall _ball;
	protected BaseGameMode _game_mode;
	protected AI_TYPE _ai_type;
	
	protected Vector2 _input_vector;
	protected bool _shoot;
	protected bool _dash;
	protected bool _jump;

	protected List< GoalLine > _op_goals;
	protected Vector3 _waypoint;

	AI_ACTIONS _curr_action;

	void Awake()
	{
		_player = GetComponent< Player > ();
	}
	
	void Start()
	{
		_game_mode = MasterManager.instance.game_mode;
		_op_goals = _game_mode.OpponentGoals (_player.teamID);
	}

	void Update()
	{
		CleanInput ();
		switch(_curr_action)
		{
		case AI_ACTIONS.IDLE:
			UpdateIdle();
			break;
		case AI_ACTIONS.PURSUE:
			UpdatePursue();
			break;
		case AI_ACTIONS.ATTACK:
			UpdateAttack();
			break;
		case AI_ACTIONS.DEFEND:
			UpdateDefend();
			break;
		case AI_ACTIONS.KINETIC:
			UpdateKinetic();
			break;
		}
	}

	void CleanInput()
	{
		_input_vector = Vector2.zero;
		_shoot = _dash = _jump = false;
		_player.InputVector (Vector2.zero);
		_player.TimePressShoot (0f);
	}

	protected virtual void UpdateIdle(){}

	protected virtual void UpdatePursue(){}
	
	protected virtual void UpdateAttack(){}

	protected virtual void UpdateDefend(){}

	protected virtual void UpdateKinetic(){}


	protected void SetAIAction(AI_ACTIONS a)
	{
		_curr_action = a;
	}

	public void AIType(AI_TYPE t)
	{
		_ai_type = t;
	}

	public AI_TYPE AIType()
	{
		return _ai_type;
	}

	public Vector2 InputVector()
	{
		return _input_vector;
	}

	public bool Shoot()
	{
		return _shoot;
	}

	public bool Dash()
	{
		return _dash;
	}

	public bool Jump()
	{
		return _jump;
	}

	protected GoalLine RandomOpGoal()
	{
		int i = Random.Range (0, _op_goals.Count);
		return _op_goals [i];
	}

	protected void InputVector(Vector3 g)
	{
		Vector3 dir = (g - transform.position).normalized;
		_input_vector.x = dir.x;
		_input_vector.y = dir.z;
		_player.InputVector (_input_vector);
	}
}

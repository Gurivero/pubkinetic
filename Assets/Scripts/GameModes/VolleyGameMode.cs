using UnityEngine;
using System.Collections;

public class VolleyGameMode : BaseGameMode {

	GoalLine[] _fields;
	Bounds[] _bounds;

	protected override void SpecificMatchConditions()
	{
		MasterManager.instance.controller_manager.control_type = ControllerManager.ECONTROL_TYPE.FREE;

		GameObject g = Instantiate< GameObject > (_game_manager.net_prefab);
		g.transform.parent = _game_manager.field.transform;
		g.transform.localPosition = new Vector3(0f, 5f, 0f);

		GoalLine[] temp = g.GetComponentsInChildren< GoalLine >();
		
		_fields = new GoalLine[temp.Length];
		_bounds  = new Bounds[temp.Length];

		foreach (GoalLine gg in temp) 
		{
			_fields[(int)gg.team_id] = gg;
			_bounds[(int)gg.team_id] = gg.GetComponent< BoxCollider >().bounds;
		}
	}

	protected override void SpecificRoundConditions(Player.EPLAYER_ID id)
	{
		_round_wait_timer = _game_manager.round_wait_timer_value;
		Player.ETEAM_ID team_id = _players [(int)id].teamID;
		Vector3 temp = _fields [(int)team_id].transform.position;
		temp.y = 5f;
		_ball.LastPlayerTouching (_players [(int)id]);
		_ball.transform.position = temp;
		_ball.GetComponent< Rigidbody >().constraints = RigidbodyConstraints.None;
	}

	public override bool ConditionsForShoot(Player p)
	{
		return BallOnPlayerSide(p);
	}

	bool BallOnPlayerSide(Player p)
	{
		Bounds b = _bounds [(int)p.teamID];
		Vector3 ball_pos_aux = _ball.transform.position;
		ball_pos_aux.y = b.center.y;
		return b.Contains (ball_pos_aux);
	}
}

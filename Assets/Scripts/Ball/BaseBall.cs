using UnityEngine;
using System.Collections;

public class BaseBall : MonoBehaviour {

	public float max_velocity;
	public float max_height_to_possess;
	public float min_vel_to_kinetic;
	public float max_vel_to_kinetic;
	public float ball_hover_lerp;
	public float min_vel_ball_steal;
	public Vector3 ball_pos_on_possess;

	bool _is_attached = false;
	bool _can_be_possessed = false;
	bool _scoring_goal = false;

	Rigidbody _rb;
	Transform _last_possessed;
	TrailRenderer _tr;

	Vector3 _nxt_hover_pos;
	Player _last_player_touched_ball;
	Player _player_with_ball;
	UnityEditor.SerializedObject _so;
	ParticleSystem _particles;

	BaseGameMode _game_mode;

	SoundManager _sm;


	// Use this for initialization
	void Awake () {
		_particles = GetComponent< ParticleSystem > ();
		ToggleParticles (false);
		_rb = GetComponent< Rigidbody > ();
		_tr = GetComponent< TrailRenderer > ();
		_tr.enabled = false;
		_so = new UnityEditor.SerializedObject(_tr);
		_nxt_hover_pos = ball_pos_on_possess;
		_nxt_hover_pos.y *= 2f;
	}

	void Start()
	{
		_sm = MasterManager.instance.sound_manager;
	}

	void Update()
	{
		if(MasterManager.instance.game_manager.pause_game)
		{
			return;
		}

		//to possess a ball, the ball must be already possessed by another player, or it has to be below the max possible height to be possessed and 
		//its velocity has to be lower than the max velocity to possess
		_can_be_possessed = 
			(transform.position.y < max_height_to_possess && _rb.velocity.magnitude < max_vel_to_kinetic)	|| IsAttached ();

		//if the ball is attached to a player freeze its rotation
//		if(_is_attached)
//		{
//			_rb.constraints = RigidbodyConstraints.FreezeRotation;
//			transform.localPosition = GetBallHoveringPosition();
//			_rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
//		}
//		else
		if(!_is_attached)
		{
			float x = Mathf.Clamp(_rb.velocity.x, - max_velocity, max_velocity);
			float y = Mathf.Clamp(_rb.velocity.y, - max_velocity, max_velocity);
			float z = Mathf.Clamp(_rb.velocity.z, - max_velocity, max_velocity);

			_rb.velocity = new Vector3(x,y,z);
		}
	}

	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(_is_attached)
		{
			_rb.velocity = new Vector3(0f,_rb.velocity.y,0f);
			_rb.angularVelocity = Vector3.zero;
		}
		else
		{
			_tr.enabled = _rb.velocity.magnitude > min_vel_to_kinetic;
			if( !_tr.enabled )
			{
				ToggleParticles(false);
			}

		}
	}

	void ToggleParticles(bool enable, Color c = new Color())
	{
		if(enable)
		{
			_particles.startColor = c;
			_particles.Play();
		}
		else
		{
			_particles.Stop();
		}
	}

	Vector3 GetBallHoveringPosition()
	{
		if(Vector3.Distance (transform.localPosition, _nxt_hover_pos) < 0.2f)
		{
			CalcNewHoverPos();
		}
		return Vector3.Lerp (transform.localPosition, _nxt_hover_pos, ball_hover_lerp);
	}


	float NewHoverYRandom()
	{
		return Random.Range(ball_pos_on_possess.y * 1.1f , ball_pos_on_possess.y * 5f);
	}

	Vector3 CalcNewHoverPos()
	{
		while(Vector3.Distance (transform.localPosition, _nxt_hover_pos) < 0.4f)
		{
			_nxt_hover_pos.y = NewHoverYRandom();
		}
		return _nxt_hover_pos;
	}

	void OnCollisionEnter(Collision collision) 
	{
		string tag = collision.gameObject.tag; 
		if(tag.Equals("Player"))
		{
			if(_game_mode.game_mode_type == GameManager.EGAME_MODE.DODGEBALL)
			{
				Player player_got_hit = collision.gameObject.GetComponent< Player >();
				Player player_shot = _last_possessed.GetComponent < Player >();
				if(player_got_hit.teamID != player_shot.teamID && !IsAttached()
				   /*&& _rb.velocity.magnitude > player_shot.max_vel_catch_dball*/)
				{
					MasterManager.instance.game_mode.ScoreGoal(player_shot.id, player_got_hit.teamID, 1);
				}
				_last_player_touched_ball = collision.gameObject.GetComponent< Player >();
			}
			else
			{
				_last_player_touched_ball = collision.gameObject.GetComponent< Player >();
				if(!IsAttached() && _last_player_touched_ball.transform != _last_possessed)
				{
					SetTrailColors(Player.EPLAYER_ID.COUNT, Player.ETEAM_ID.COUNT);
				}
			}
		}
		else if(tag.Equals("Wall"))
		{
			GameObject g = GameObject.Instantiate(MasterManager.instance.game_manager.ball_hit_prefab);
			g.transform.position = collision.contacts[0].point;
			g.transform.LookAt(Camera.main.transform.position);
			_sm.PlaySound(SoundManager.ESOUNDS.WALL_HIT);
		}
	}


	void OnTriggerEnter(Collider other)
	{
		if(other.tag.Equals("Goal"))
		{
			ScoreGoal(other);
		}
	}

	void OnTriggerStay(Collider other)
	{
		if(_game_mode.game_mode_type == GameManager.EGAME_MODE.VOLLEY)
		{
			float vel_mag = _rb.velocity.magnitude;
			float y_vel = _rb.velocity.y;
			if(
				(vel_mag < 0.01f || Mathf.Abs(y_vel) < 0.001f && vel_mag < 5f)
			   && other.GetComponent< GoalLine >() != null)
			{
				ScoreGoal(other);
			}
		}
	}
	
	void ScoreGoal(Collider goal)
	{
		if(!_scoring_goal)
		{
			_scoring_goal = true;
			GoalLine gl = goal.GetComponent< GoalLine >();	
			MasterManager.instance.game_mode.ScoreGoal(_last_player_touched_ball.id, gl.team_id, gl.points_given);
		}
	}
	
	public void UnPossess()
	{
		_rb.constraints = RigidbodyConstraints.None;
		transform.SetParent(null);
		_is_attached = false;
		_game_mode.PlayerIDWithBall( Player.EPLAYER_ID.COUNT);
		_rb.useGravity = true;
	}

	public void Reset()
	{
		_scoring_goal = false;
	}

	public Player.ETEAM_ID TeamWithBallID()
	{
		if(_player_with_ball != null)
		{
			return _player_with_ball.teamID;
		}
		else
		{
			return Player.ETEAM_ID.COUNT;
		}
	}

	public void SetTrailColors(Player.EPLAYER_ID id, Player.ETEAM_ID team_id)
	{
		Color c = MasterManager.instance.game_manager.player_colors [(int)id];
		Color c1 = c, c2 = c, c3 = c, c4 = c, c5= c;
		c1.a = 100f;
		c1.a = 50f;
		c1.a = 25f;
		c1.a = 10f;
		c1.a = 0f;

		_so.FindProperty("m_Colors.m_Color[0]").colorValue= c1;
		_so.FindProperty("m_Colors.m_Color[1]").colorValue = c2;
		_so.FindProperty("m_Colors.m_Color[2]").colorValue = c3;
		_so.FindProperty("m_Colors.m_Color[3]").colorValue = c4;
		_so.FindProperty("m_Colors.m_Color[4]").colorValue = c5;
		_so.ApplyModifiedProperties();
		
		if(id == Player.EPLAYER_ID.COUNT)
		{
			ToggleParticles(false);
		}
		else if( _particles.isStopped)
		{
			ToggleParticles(true, c);
		}
	}

	public bool CanBePossessed()
	{
		return _can_be_possessed;
	}

	public bool IsAttached()
	{
		return _is_attached;
	}


	public void Possess(Transform t, Player.EPLAYER_ID id)
	{
		if(transform.parent != null)
		{
			//if the ball is already possessed and some other player touches the ball, the new player steals the ball
			if(t.GetComponent< Rigidbody >().velocity.magnitude > min_vel_ball_steal)
			{
				transform.parent.GetComponent< Player >().BallStolen();
			}
			else
			{
				return;
			}
		}

		Color c = MasterManager.instance.game_manager.player_colors [(int)id];
		ToggleParticles (true, c);
		transform.SetParent (t);
		_is_attached = true; 

		//set these physics constraints to stop the ball from spinning when possessed
		_rb.angularVelocity = Vector3.zero;
		transform.localPosition = GetBallHoveringPosition();
		_rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;


		_last_possessed = t;
		_game_mode.PlayerIDWithBall(id);
		transform.localPosition = new Vector3 (ball_pos_on_possess.x, transform.localPosition.y, ball_pos_on_possess.z);
		_tr.enabled = false;

		//when possessed, the ball hovers in front of the possessing player. Get the next hovering position
		_nxt_hover_pos = CalcNewHoverPos ();
		_rb.useGravity = false;
		_player_with_ball = t.GetComponent< Player > ();
	}

	public void LastPlayerTouching(Player p)
	{
		_last_player_touched_ball = p;
	}

	public bool IsParent(Transform t)
	{
		return t == transform.parent;
	}

	public bool CanKineticControl(Transform t)
	{
		return _rb.velocity.magnitude > min_vel_to_kinetic 
			&& transform.parent == null
			&& _last_possessed == t
			&& _last_player_touched_ball.transform == t;
	}

	public void GameMode(BaseGameMode gm)
	{
		_game_mode = gm;
	}
}
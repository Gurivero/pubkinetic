using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreBoard : MonoBehaviour {

	public Text[] scores;
	public Text[] ball_timers;

	public void UpdateScoreboard()
	{
		switch(MasterManager.instance.GameModeType())
		{
		case GameManager.EGAME_MODE.FFA:
			UpdateScoreboardFFA();
			break;
		case GameManager.EGAME_MODE.MATCH:
		case GameManager.EGAME_MODE.TOURNAMENT:
		//case GameManager.EGAME_MODE.FREEZE:
		case GameManager.EGAME_MODE.VOLLEY:
		case GameManager.EGAME_MODE.DODGEBALL:
			UpdateScoreboardMatch();
			break;
		default:
			Debug.Log("ERROR: UpdateScoreboard()");
			break;
		}
	}

	void UpdateScoreboardFFA()
	{
		BaseGameMode game_mode = MasterManager.instance.game_mode;
		for(int i = 0; i < scores.Length ; ++i)
		{
			scores[i].text = game_mode.Goals((Player.EPLAYER_ID)i).ToString();;
		}
	}
	
	void UpdateScoreboardMatch()
	{
		BaseGameMode game_mode = MasterManager.instance.game_mode;

		string goals_t1 = game_mode.Goals(Player.ETEAM_ID.TEAM_1).ToString();
		string goals_t2 = game_mode.Goals(Player.ETEAM_ID.TEAM_2).ToString();

		scores [0].text = goals_t1 + " - " + goals_t2;
	}

	public void UpdateBallTimer()
	{
		switch(MasterManager.instance.GameModeType())
		{
		case GameManager.EGAME_MODE.FFA:
			UpdateBallTimerFFA();
			break;
		case GameManager.EGAME_MODE.MATCH:
		case GameManager.EGAME_MODE.TOURNAMENT:
		//case GameManager.EGAME_MODE.FREEZE:
		case GameManager.EGAME_MODE.VOLLEY:
		case GameManager.EGAME_MODE.DODGEBALL:
			UpdateBallTimerMatch();
			break;
		default:
			Debug.Log("ERROR: UpdateScoreboard()");
			break;
		}
	}

	void UpdateBallTimerMatch()
	{
		bool ball_possessed = MasterManager.instance.game_mode.ball.IsAttached ();

		if(ball_possessed)
		{
			int idx = (int)MasterManager.instance.game_mode.ball.TeamWithBallID();
			int opp = idx == 0 ? 1 : 0;
			ball_timers [idx].text = MasterManager.instance.game_mode.BallTimer ().ToString ();
			ball_timers [opp].text = "-";
		}
		else
		{
			ball_timers [0].text = "-";
			ball_timers [1].text = "-";
		}



	}

	void UpdateBallTimerFFA()
	{
		bool ball_possessed = MasterManager.instance.game_mode.ball.IsAttached ();

		if(ball_possessed)
		{
			ball_timers [0].text = MasterManager.instance.game_mode.BallTimer ().ToString ();
		}
		else
		{
			ball_timers [0].text = "-";
		}
	}
}

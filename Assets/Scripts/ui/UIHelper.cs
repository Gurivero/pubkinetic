﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIHelper : MonoBehaviour {

	public GameObject helper;


	// Use this for initialization
	void Start () {
		helper.SetActive (false);
		GetComponent< Button > ().interactable = false;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void ToggleHelper(bool val)
	{
		helper.SetActive (val);
	}
}

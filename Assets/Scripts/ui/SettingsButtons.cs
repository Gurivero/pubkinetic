﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SettingsButtons : MonoBehaviour {

	public Text music_button;
	public Text sound_button;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{
		music_button.text = MasterManager.instance.sound_manager.GetMusicButtonText ();
		sound_button.text = MasterManager.instance.sound_manager.GetSoundButtonText ();
	}
}
